<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'nick_name' => $faker->firstName,
        'age' => $faker->numberBetween(15,50),
        'birth_date' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y-m-d'),
        'address' => $faker->address,
        'mobile' => $faker->phoneNumber,
        'create_by' => 1,
        'modify_by' => 1,
    ];
});

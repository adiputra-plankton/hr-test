<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', 'Api\UsersController@login');
    Route::post('/register', 'Api\UsersController@register');
    Route::get('/logout', 'Api\UsersController@logout')->middleware('auth:api');

    Route::get('/profile', 'Api\UsersController@profile')->middleware('auth:api');

    Route::post('/forgot_password', 'Api\AuthController@forgotPassword');


    Route::group(['prefix' => '/employees', 'middleware' => 'auth:api'], function () {
        Route::get('/', 'Api\EmployeeController@index');
        Route::get('/{employeeId}', 'Api\EmployeeController@show');
        Route::post('/', 'Api\EmployeeController@store');
        Route::put('/{employeeId}', 'Api\EmployeeController@update');
        Route::delete('/{employeeId}', 'Api\EmployeeController@destroy');
    });
});

Laravel Test Employee CRUD

## How To Install
This project under laravel 6.*. Please follow requirement of that version.

And in listing we implement Cache tags meaning we cant use file cache to handle this list. You need to install redis.

- Running command `php composer install`
- Setup Passport and Auth Migration
- We can use seeder by calling command ``php artisan db:seed`` to make example employee data
- Open file `.env` and Change `CACHE_DRIVER` to REDIS

Document API: [https://documenter.getpostman.com/view/988225/SztEY6hd?version=latest](Postman)
 

<?php

namespace App\Http\Services\Employee;

use App\Employee;
use App\Http\Repositories\Employee\EmployeeRepository;
use App\User;

class EmployeeService
{
    protected $repository;
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->repository = new EmployeeRepository(new Employee());
    }

    public function getList($page, $search="", $limit=10, $sortKey=null, $sortValue=null)
    {
        $sortKey = $sortKey ?: 'id';
        $sortValue = $sortValue ?: 'asc';

        $employeeCacheService = new EmployeeCacheService();
        $cacheKey = $employeeCacheService->getPrefixCacheList($page, $search, $limit, $sortKey, $sortValue);

        $employees = $employeeCacheService->getByKey($cacheKey);

        if (!$employees) {
            $employees = $this->repository->getListPaginate($search, $limit, $sortKey, $sortValue);

            if ($employees)
                $employeeCacheService->store($cacheKey, $employees);

            return $employees;
        }

        return $employees;
    }

    public function getById(int $employeeId)
    {
        $employee = $this->repository->getById($employeeId);

        if (!$employee)
            throw new \Exception("Employee not found", 404);

        return $employee;
    }

    public function saveEmployeeFromRequest($request)
    {
        $arrayData = $this->convertRequestObjectToArray($request);

        $arrayData['create_by'] = $this->user->id;

        $employee = $this->repository->save($arrayData);

        $employeeCacheService = new EmployeeCacheService();
        $employeeCacheService->resetCache();

        return $employee;
    }

    private function convertRequestObjectToArray($request)
    {
        $arrayData['full_name'] = $request->full_name;
        $arrayData['nick_name'] = $request->nick_name;
        $arrayData['age'] = $request->age;
        $arrayData['birth_date'] = $request->birth_date;
        $arrayData['address'] = $request->address;
        $arrayData['mobile'] = $request->mobile;
        $arrayData['avatar'] = $request->avatar ?? '';
        $arrayData['create_by'] = $this->user->id;

        return $arrayData;
    }

    public function updateEmployeeById(int $employeeId, $request)
    {
        $employee = $this->getById($employeeId);

        if (!$employee)
            throw new \Exception("Employee not found", 404);

        $arrayData = $this->convertRequestObjectToArray($request);

        $arrayData['modify_by'] = $this->user->id;

        $updated = $this->repository->update($employeeId, $arrayData);

        if (!$updated)
            throw new \Exception("Update failed", 500);

        $employeeCacheService = new EmployeeCacheService();
        $employeeCacheService->resetCache();

        return $this->getById($employeeId);
    }

    public function deleteEmployee(int $employeeId)
    {
        $employee = $this->getById($employeeId);

        if (!$employee)
            throw new \Exception("Employee not found", 404);

        $employee = $this->repository->delete($employeeId);

        $employeeCacheService = new EmployeeCacheService();
        $employeeCacheService->resetCache();

        return $employee;
    }
}

<?php

namespace App\Http\Services\Employee;

use Illuminate\Support\Facades\Cache;

class EmployeeCacheService
{
    const CACHE_PREFIX = 'employee_filter_';
    const CACHE_TAG = 'employee';

    public function getPrefixCacheList($page, $search, $limit, $sortKey, $sortValue)
    {
        $cleanSearch = preg_replace('/\s+/', '_', $search);

        $cacheKeyId =  $page .'_'. $cleanSearch .'_'. $limit .'_'. $sortKey .'_'. $sortValue;

        $cacheKey = self::CACHE_PREFIX . $cacheKeyId;

        return $cacheKey;
    }

    public function resetCache()
    {
        Cache::tags([self::CACHE_TAG])->flush();
    }

    public function getByKey($key)
    {
        return Cache::tags([self::CACHE_TAG])->get($key);
    }

    public function store($key, $data)
    {
        Cache::tags([self::CACHE_TAG])->put($key, $data, now()->addMinutes(60));
    }
}

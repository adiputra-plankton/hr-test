<?php

namespace App\Http\Resources\Employee;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EmployeeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => EmployeeItem::collection($this->collection),
            'query_params' => [
                'search' => $request->search ?? '',
                'limit' => $request->limit ?? 10,
                'sort_key' => $request->sort_key ?: 'id',
                'sort_value' => $request->sort_value ?: 'asc'
            ]
        ];
    }
}

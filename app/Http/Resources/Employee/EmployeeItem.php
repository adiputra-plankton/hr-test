<?php

namespace App\Http\Resources\Employee;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'nick_name' => $this->nick_name,
            'age' => $this->age,
            'birth_date' => $this->birth_date,
            'address' => $this->address,
            'mobile' => $this->mobile,
            'avatar' => $this->avatar,
            'create_by' => $this->create_by,
            'created_at' => $this->created_at,
            'modify_by' => $this->modify_by,
            'updated_at' => $this->updated_at,
       ];
    }
}

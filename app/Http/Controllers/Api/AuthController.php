<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function forgotPassword(Request $request)
    {
        $rules = array(
            'email' => "required|email",
        );

        $validator = Validator::make(['email' => $request->email], $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
            ], 422);
        } else {
            try {
                $response = Password::sendResetLink(['email' => $request->email], function (Message $message) {
                    $message->subject($this->getEmailSubject());
                });

                switch ($response) {
                    case Password::RESET_LINK_SENT:
                        return response()->json([
                            'success' => true,
                            'message' => $response,
                        ], 200);
                    case Password::INVALID_USER:
                        return response()->json([
                            'success' => false,
                            'message' => $response,
                        ], 400);
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'success' => false,
                    'message' => $ex->getMessage(),
                ], 400);
            }
        }
    }
}

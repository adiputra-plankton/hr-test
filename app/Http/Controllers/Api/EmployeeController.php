<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\Employee\EmployeeCollection;
use App\Http\Resources\Employee\EmployeeItem;
use App\Http\Services\Employee\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $employeeService = new EmployeeService(Auth::user());

        $search = $request->search ?: '';
        $limit = $request->limit ?: 10;
        $sortKey = $request->sort_key ?: 'id';
        $sortValue = $request->sort_value ?: null;
        $page = $request->page ?: 1;

        $employees = $employeeService->getList($page, $search, $limit, $sortKey, $sortValue);

        return new EmployeeCollection($employees);
    }

    public function show(Request $request)
    {
        $employeeId = $request->route('employeeId');

        $employeeService = new EmployeeService(Auth::user());

        $employee = $employeeService->getById($employeeId);

        return new EmployeeItem($employee);
    }

    public function store(EmployeeRequest $request)
    {
        $employeeService = new EmployeeService(Auth::user());

        $employee = $employeeService->saveEmployeeFromRequest($request);

        return new EmployeeItem($employee);
    }

    public function update(EmployeeRequest $request)
    {
        $employeeId = $request->route('employeeId');

        $employeeService = new EmployeeService(Auth::user());

        $employee = $employeeService->updateEmployeeById($employeeId, $request);

        return new EmployeeItem($employee);
    }

    public function destroy(Request $request)
    {
        $employeeId = $request->route('employeeId');

        $employeeService = new EmployeeService(Auth::user());

        $employeeService->deleteEmployee($employeeId);

        return response()->json([
            'success' => true,
            'message' => 'Success deleted'
        ]);
    }
}

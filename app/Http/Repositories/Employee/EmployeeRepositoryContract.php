<?php

namespace App\Http\Repositories\Employee;

interface EmployeeRepositoryContract
{
    function getAll();
    function getById(int $id);
    function update(int $id, array $data);
    function save(array $data);
    function delete(int $id);
}

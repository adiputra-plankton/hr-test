<?php

namespace App\Http\Repositories\Employee;

use App\Employee;

class EmployeeRepository implements EmployeeRepositoryContract
{
    protected $model;

    public function __construct(Employee $employee)
    {
        $this->model = $employee;
    }

    function getAll()
    {
        return $this->model->get();
    }

    function getById(int $id)
    {
        return $this->model->find($id);
    }

    public function save(array $data)
    {
        return $this->model->create($data);
    }

    public function update(int $id, array $data)
    {
        return $this->model
            ->where('id', $id)
            ->update($data);
    }

    public function delete(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->delete();
    }

    public function getListPaginate($search='', $limit=10, $sortKey, $sortValue)
    {
        return $this->model
            ->when(($search != ""), function ($query) {
                return $query->where('full_name', 'like', '%$search%');
            })
            ->orderBy($sortKey, $sortValue)
            ->paginate($limit);
    }
}

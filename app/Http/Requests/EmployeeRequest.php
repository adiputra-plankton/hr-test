<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|max:255',
            'nick_name' => 'required',
            'age' => 'required|numeric',
            'birth_date' => 'required|date',
            'address' => 'required|min:10',
            'mobile' => 'required|min:10',
            'avatar' => 'nullable|min:10', # base_64
        ];
    }
}
